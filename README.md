[logo]: https://gitlab.com/aljurado/packaging-inkscape-extensions/-/raw/master/Logo.svg "Logo"
![logo][logo]
# Packaging Inkscape 1.0 Extensions

## Description
This collection is a set of extensions geared towards the world of packaging to help designers create die-cut profiles of their designs.

With this collection you can generate the profiles of different cases, trays, folders, etc ... to use in your designs.

Initially, this collection includes 3 types of basic cases: linear, automatic funds and trays. Altogether 7 types of different cases.

* Linear: Girdles, Normal or opposite leg linear cases, Plane or parallel leg type linear cases and semi-automatic or Swiss bottom cases.
* Automatic bottom cases
* Trays: shirt-type or double rail type, 4P tray.

These are the first 6 extensions, and although the cases generated are of the correct size and dimensions, they have some technical deficiencies that will be solved in future modifications, but which are not decisive for the design.

The next extensions to be added to this collection will be 4 and 6 point trays and folders with 1 and 2 pockets, with and without spine.

Among the corrections to techniques made more immediately are the substitution of some vertices for rounded corners. Add the option to offset the profile measurements based on the thickness of the material. And adding other work units like centimeter and inch, currently only works in mm.

## How to use these extensions
To use these extensions download all the files in the folder "share\inkscape\extensions" and restart Inkscape. Usually, in Windows computers usually "c:\Program Files\Inkscape\share\inkscape\extensions", in Linux based systems at your home folder ".config\inkscape\extensions". Just download all the repo in a zip file using the dowload button, then unzip it in the extensions folder.

**Take into account that these extensions have been created directly for version 1.0 of Inkscape, just in this version the extension system was changed so that these are not valid for previous versions.**


## License
This collection of extensions is licensed under GPL v3. ([GPL v3](https://gitlab.com/aljurado/packaging-inkscape-extensions/-/blob/master/LICENSE))

---

![logo][logo]
# Packaging Inkscape 1.0 Extensions

## Descripción
Esta colección es un conjunto de extensiones orientadas al mundo del packaging para ayudar a los diseñadores a crear los perfiles de troquealdo de sus diseños.

Con esta colección puedes generar los perfiles de diferentes estuches, bandejas, carpetas, etc... para usar en tus diseños.

De forma inicial esta colección incluye 3 tipos de estuches básicos: lineales, fondos automáticos y bandejas. En total 7 tipos de estuches diferentes.

* Lineales: Fajas, Estuches lineales normales o de patas opuestas, Estuches lineales tipo avión o de patas paralelas y estuches fondo semi-automático o de fondo Suizo.
* Estuches de fondo automático
* Bandejas: tipo camiseras o de doble baranda, bases de 4 puntos.

Estas son los primeras 6 extensiones, y aunque los estuches generados son del tamaño y dimensiones correctas, tienen algunas deficiencias técnicas que se iran resolviendo en futuras revisiones pero que para el diseño no son determiantes.

Las próximas extensiones que se añadirán a esta colección serán bandejas de 4 y 6 puntos y carpetas con 1 y 2 bolsillos, con y sin lomo.

Entre las correcciones a técnicas realizar de forma más inmediata están la sustitución de algunos vértices por esquinas redondeadas. Agregar la opción de compensar las medidas del perfil en función del grueso del material. Y agregar otras unidades de trabajo como el centímetro y la pulgada, actualmente solo trabaja en milímetros.

## Como usar estas extensiones
Para usar estas extensiones descarga todos los archivos en la carpeta de extensiones y reinicia Inkscape. Generalmente en los equipos con Windows en "c:\Archivos de programa\Inkscape\share\Inkscape\extensions\". Para los archivos basados en Linux en tu carpeta home ".config\inkscape\extensions". Para descargar los archivos usa el botón de arriba y descarga todo el repositorio en formato zip y descomprimelo en la carpeta de extensiones.

**Ten en cuenta que estas extensiones se han creado directamente para la versión 1.0 de Inkscape, justo en esta versión se cambio el sistema de extensiones con lo que estas no son validas para versiones anteriores.**


## Licencia
Esta colección de extensiones está licenciada bajo GPL v3. ([GPL v3](https://gitlab.com/aljurado/packaging-inkscape-extensions/-/blob/master/LICENSE))
